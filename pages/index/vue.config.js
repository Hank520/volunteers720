// vue.config.js
module.exports = {
  devServer: {
    proxy: {
      '/prefix/api/user/list': {
        // target: 'https://api.weixin.qq.com',
        pathRewrite: {
          '^/prefix': ''
        }
      }
    },
  }
}
