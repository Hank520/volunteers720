var location = [{
	"code": "410000",
	"name": "河南省",
	"level": "province",
	"children": [{
			"code": "410600",
			"name": "鹤壁市",
			"level": "city",
			"children": [{
					"code": "410622",
					"name": "淇县",
					"level": "district",
					"children": [{
							"code": "410622",
							"name": "灵山街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410622",
							"name": "桥盟街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410622",
							"name": "卫都街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410622",
							"name": "庙口镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410622",
							"name": "黄洞乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410622",
							"name": "朝歌街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410622",
							"name": "北阳镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410622",
							"name": "高村镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410622",
							"name": "西岗镇",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410621",
					"name": "浚县",
					"level": "district",
					"children": [{
							"code": "410621",
							"name": "小河镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410621",
							"name": "善堂镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410621",
							"name": "新镇镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410621",
							"name": "浚州街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410621",
							"name": "卫贤镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410621",
							"name": "伾山街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410621",
							"name": "黎阳街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410621",
							"name": "屯子镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410621",
							"name": "白寺镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410621",
							"name": "王庄镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410621",
							"name": "卫溪街道",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410603",
					"name": "山城区",
					"level": "district",
					"children": [{
							"code": "410603",
							"name": "大胡街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410603",
							"name": "汤河街街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410603",
							"name": "长风中路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410603",
							"name": "宝山街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410603",
							"name": "石林镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410603",
							"name": "山城路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410603",
							"name": "红旗街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410603",
							"name": "鹿楼街道",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410602",
					"name": "鹤山区",
					"level": "district",
					"children": [{
							"code": "410602",
							"name": "中山路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410602",
							"name": "姬家山乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410602",
							"name": "九矿广场街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410602",
							"name": "新华街街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410602",
							"name": "中北街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410602",
							"name": "鹤壁集镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410602",
							"name": "鹤山街街道",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410611",
					"name": "淇滨区",
					"level": "district",
					"children": [{
							"code": "410611",
							"name": "金山街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410611",
							"name": "上峪乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410611",
							"name": "大赉店镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410611",
							"name": "大河涧乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410611",
							"name": "东杨街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410611",
							"name": "钜桥镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410611",
							"name": "长江路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410611",
							"name": "黎阳路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410611",
							"name": "渤海路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410611",
							"name": "海河路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410611",
							"name": "泰山路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410611",
							"name": "九州路街道",
							"level": "street",
							"children": []
						}
					]
				}
			]
		},
		{
			"code": "410100",
			"name": "郑州市",
			"level": "city",
			"children": [{
					"code": "410185",
					"name": "登封市",
					"level": "district",
					"children": [{
							"code": "410185",
							"name": "唐庄镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410185",
							"name": "大冶镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410185",
							"name": "石道乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410185",
							"name": "颖阳镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410185",
							"name": "白坪乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410185",
							"name": "君召乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410185",
							"name": "徐庄镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410185",
							"name": "少林街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410185",
							"name": "大金店镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410185",
							"name": "阳城区镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410185",
							"name": "宣化镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410185",
							"name": "中岳街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410185",
							"name": "东华镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410185",
							"name": "嵩阳街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410185",
							"name": "送表矿区",
							"level": "street",
							"children": []
						},
						{
							"code": "410185",
							"name": "告成镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410185",
							"name": "卢店镇",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410183",
					"name": "新密市",
					"level": "district",
					"children": [{
							"code": "410183",
							"name": "大隗镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410183",
							"name": "苟堂镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410183",
							"name": "西大街街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410183",
							"name": "青屏街街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410183",
							"name": "平陌镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410183",
							"name": "刘寨镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410183",
							"name": "白寨镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410183",
							"name": "郑州曲梁产业集聚区",
							"level": "street",
							"children": []
						},
						{
							"code": "410183",
							"name": "来集镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410183",
							"name": "袁庄乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410183",
							"name": "尖山风景区",
							"level": "street",
							"children": []
						},
						{
							"code": "410183",
							"name": "新华路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410183",
							"name": "矿区街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410183",
							"name": "城关镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410183",
							"name": "曲梁镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410183",
							"name": "超化镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410183",
							"name": "岳村镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410183",
							"name": "牛店镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410183",
							"name": "米村镇",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410184",
					"name": "新郑市",
					"level": "district",
					"children": [{
							"code": "410184",
							"name": "八千乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410184",
							"name": "龙王乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410184",
							"name": "郭店镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410184",
							"name": "中心城区新区建设管理委员会",
							"level": "street",
							"children": []
						},
						{
							"code": "410184",
							"name": "新港街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410184",
							"name": "和庄镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410184",
							"name": "新烟街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410184",
							"name": "具茨山国家级森林公园",
							"level": "street",
							"children": []
						},
						{
							"code": "410184",
							"name": "龙湖镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410184",
							"name": "新华路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410184",
							"name": "新建路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410184",
							"name": "银河街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410184",
							"name": "孟庄镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410184",
							"name": "新村镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410184",
							"name": "郑港街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410184",
							"name": "观音寺镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410184",
							"name": "梨河镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410184",
							"name": "城关乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410184",
							"name": "辛店镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410184",
							"name": "薛店镇",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410106",
					"name": "上街区",
					"level": "district",
					"children": [{
							"code": "410106",
							"name": "中心路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410106",
							"name": "济源路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410106",
							"name": "工业路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410106",
							"name": "峡窝镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410106",
							"name": "新安路街道",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410122",
					"name": "中牟县",
					"level": "district",
					"children": [{
							"code": "410122",
							"name": "狼城岗镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "大孟街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "东风路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "八岗镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "广惠街街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "白沙镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "刘集镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "姚家镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "三官庙镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "刁家乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "雁鸣湖镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "韩寺镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "万滩镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "滨河街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "张庄镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "黄店镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "九龙镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "前程街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "杨桥街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "官渡镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "青年路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "豫兴街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "郑庵镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410122",
							"name": "祥云街道",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410108",
					"name": "惠济区",
					"level": "district",
					"children": [{
							"code": "410108",
							"name": "古荥镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410108",
							"name": "刘寨街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410108",
							"name": "大河路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410108",
							"name": "长兴路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410108",
							"name": "迎宾路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410108",
							"name": "老鸦陈街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410108",
							"name": "新城街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410108",
							"name": "花园口镇",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410181",
					"name": "巩义市",
					"level": "district",
					"children": [{
							"code": "410181",
							"name": "康店镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410181",
							"name": "站街镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410181",
							"name": "西村镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410181",
							"name": "永安路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410181",
							"name": "小关镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410181",
							"name": "夹津口镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410181",
							"name": "杜甫路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410181",
							"name": "回郭镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410181",
							"name": "河洛镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410181",
							"name": "北山口镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410181",
							"name": "竹林镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410181",
							"name": "大峪沟镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410181",
							"name": "米河镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410181",
							"name": "芝田镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410181",
							"name": "涉村镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410181",
							"name": "孝义街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410181",
							"name": "新中镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410181",
							"name": "鲁庄镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410181",
							"name": "新华路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410181",
							"name": "紫荆路街道",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410103",
					"name": "二七区",
					"level": "district",
					"children": [{
							"code": "410103",
							"name": "长江路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410103",
							"name": "铭功路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410103",
							"name": "京广路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410103",
							"name": "福华街街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410103",
							"name": "德化街街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410103",
							"name": "大学路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410103",
							"name": "马寨镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410103",
							"name": "一马路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410103",
							"name": "蜜蜂张街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410103",
							"name": "建中街街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410103",
							"name": "侯寨街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410103",
							"name": "淮河路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410103",
							"name": "解放路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410103",
							"name": "五里堡街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410103",
							"name": "人和路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410103",
							"name": "嵩山路街道",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410182",
					"name": "荥阳市",
					"level": "district",
					"children": [{
							"code": "410182",
							"name": "高山镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410182",
							"name": "崔庙镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410182",
							"name": "豫龙镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410182",
							"name": "城关乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410182",
							"name": "贾峪镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410182",
							"name": "刘河镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410182",
							"name": "京城路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410182",
							"name": "广武镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410182",
							"name": "索河街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410182",
							"name": "王村镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410182",
							"name": "高村乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410182",
							"name": "金寨回族乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410182",
							"name": "汜水镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410182",
							"name": "乔楼镇",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410102",
					"name": "中原区",
					"level": "district",
					"children": [{
							"code": "410102",
							"name": "梧桐街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410102",
							"name": "建设路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410102",
							"name": "中原西路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410102",
							"name": "棉纺路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410102",
							"name": "沟赵乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410102",
							"name": "航海西路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410102",
							"name": "石佛镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410102",
							"name": "须水街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410102",
							"name": "汝河路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410102",
							"name": "枫杨街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410102",
							"name": "秦岭路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410102",
							"name": "双桥街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410102",
							"name": "西流湖街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410102",
							"name": "桐柏路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410102",
							"name": "三官庙街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410102",
							"name": "绿东村街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410102",
							"name": "林山寨街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410102",
							"name": "莲湖街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410102",
							"name": "柳湖街道",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410104",
					"name": "管城回族区",
					"level": "district",
					"children": [{
							"code": "410104",
							"name": "陇海马路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410104",
							"name": "二里岗街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410104",
							"name": "北下街街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410104",
							"name": "南关街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410104",
							"name": "经济开发区明湖街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410104",
							"name": "城东路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410104",
							"name": "圃田乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410104",
							"name": "十八里河镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410104",
							"name": "经济开发区潮河街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410104",
							"name": "航海东路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410104",
							"name": "东大街街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410104",
							"name": "紫荆山南路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410104",
							"name": "商都路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410104",
							"name": "西大街街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410104",
							"name": "京航街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410104",
							"name": "南曹街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410104",
							"name": "金岱街道",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410105",
					"name": "金水区",
					"level": "district",
					"children": [{
							"code": "410105",
							"name": "丰庆路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "龙湖街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "东风路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "杨金路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "未来路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "大石桥街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "北林路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "龙源路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "祭城路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "人民路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "龙子湖街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "南阳新村街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "经八路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "如意湖街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "花园路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "杜岭街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "国基路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "博学路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "文化路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "南阳路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "凤凰台街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "丰产路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "兴达路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410105",
							"name": "金光路街道",
							"level": "street",
							"children": []
						}
					]
				}
			]
		},
		{
			"code": "410500",
			"name": "安阳市",
			"level": "city",
			"children": [{
					"code": "410505",
					"name": "殷都区",
					"level": "district",
					"children": [{
							"code": "410505",
							"name": "相台街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410505",
							"name": "铁西路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410505",
							"name": "清风街街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410505",
							"name": "北蒙街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410505",
							"name": "纱厂路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410505",
							"name": "梅园庄街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410505",
							"name": "西郊乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410505",
							"name": "电厂路街道",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410527",
					"name": "内黄县",
					"level": "district",
					"children": [{
							"code": "410527",
							"name": "楚旺镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410527",
							"name": "高堤乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410527",
							"name": "井店镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410527",
							"name": "城关镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410527",
							"name": "后河镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410527",
							"name": "东庄镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410527",
							"name": "六村乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410527",
							"name": "石盘屯乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410527",
							"name": "中召乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410527",
							"name": "亳城镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410527",
							"name": "田氏镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410527",
							"name": "马上乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410527",
							"name": "梁庄镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410527",
							"name": "豆公镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410527",
							"name": "二安镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410527",
							"name": "张龙乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410527",
							"name": "宋村乡",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410506",
					"name": "龙安区",
					"level": "district",
					"children": [{
							"code": "410506",
							"name": "太行小区街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410506",
							"name": "田村街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410506",
							"name": "东风乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410506",
							"name": "彰武街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410506",
							"name": "龙泉镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410506",
							"name": "文昌大道街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410506",
							"name": "文明大道街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410506",
							"name": "中州路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410506",
							"name": "马投涧镇",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410522",
					"name": "安阳县",
					"level": "district",
					"children": [{
							"code": "410522",
							"name": "磊口乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410522",
							"name": "永和镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410522",
							"name": "铜冶镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410522",
							"name": "马家乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410522",
							"name": "蒋村镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410522",
							"name": "北郭乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410522",
							"name": "伦掌镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410522",
							"name": "都里镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410522",
							"name": "瓦店乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410522",
							"name": "安丰乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410522",
							"name": "许家沟乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410522",
							"name": "辛村镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410522",
							"name": "水冶镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410522",
							"name": "韩陵镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410522",
							"name": "曲沟镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410522",
							"name": "善应镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410522",
							"name": "崔家桥镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410522",
							"name": "洪河屯乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410522",
							"name": "吕村镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410522",
							"name": "白璧镇",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410503",
					"name": "北关区",
					"level": "district",
					"children": [{
							"code": "410503",
							"name": "曙光路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410503",
							"name": "洹北街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410503",
							"name": "民航路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410503",
							"name": "豆腐营街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410503",
							"name": "红旗路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410503",
							"name": "彰东街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410503",
							"name": "灯塔路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410503",
							"name": "解放路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410503",
							"name": "柏庄镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410503",
							"name": "彰北街道",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410581",
					"name": "林州市",
					"level": "district",
					"children": [{
							"code": "410581",
							"name": "龙山街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410581",
							"name": "临淇镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410581",
							"name": "采桑镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410581",
							"name": "陵阳镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410581",
							"name": "合涧镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410581",
							"name": "东岗镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410581",
							"name": "桂园街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410581",
							"name": "东姚镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410581",
							"name": "河顺镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410581",
							"name": "茶店镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410581",
							"name": "黄华镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410581",
							"name": "原康镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410581",
							"name": "振林街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410581",
							"name": "开元街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410581",
							"name": "桂林镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410581",
							"name": "横水镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410581",
							"name": "五龙镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410581",
							"name": "姚村镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410581",
							"name": "石板岩镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410581",
							"name": "任村镇",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410523",
					"name": "汤阴县",
					"level": "district",
					"children": [{
							"code": "410523",
							"name": "城关镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410523",
							"name": "宜沟镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410523",
							"name": "瓦岗乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410523",
							"name": "任固镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410523",
							"name": "五陵镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410523",
							"name": "伏道镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410523",
							"name": "古贤镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410523",
							"name": "菜园镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410523",
							"name": "韩庄镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410523",
							"name": "白营镇",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410526",
					"name": "滑县",
					"level": "district",
					"children": [{
							"code": "410526",
							"name": "枣村乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "桑村乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "瓦岗寨乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "上官镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "小铺乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "焦虎镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "留固镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "慈周寨镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "锦和街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "王庄镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "老店镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "老爷庙乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "大寨乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "赵营镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "高平镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "半坡店镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "牛屯镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "万古镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "城关街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "道口镇街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "八里营镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "四间房镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410526",
							"name": "白道口镇",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410502",
					"name": "文峰区",
					"level": "district",
					"children": [{
							"code": "410502",
							"name": "宝莲寺镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410502",
							"name": "光华路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410502",
							"name": "开发区银杏大街街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410502",
							"name": "东关街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410502",
							"name": "北大街街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410502",
							"name": "紫薇大道街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410502",
							"name": "永明路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410502",
							"name": "开发区峨嵋大街街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410502",
							"name": "北大街综合治理办公室",
							"level": "street",
							"children": []
						},
						{
							"code": "410502",
							"name": "西关街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410502",
							"name": "步行街综合治理办公室",
							"level": "street",
							"children": []
						},
						{
							"code": "410502",
							"name": "头二三街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410502",
							"name": "南关街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410502",
							"name": "开发区商颂大街街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410502",
							"name": "甜水井街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410502",
							"name": "高庄镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410502",
							"name": "东大街街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410502",
							"name": "中华路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410502",
							"name": "西大街街道",
							"level": "street",
							"children": []
						}
					]
				}
			]
		},
		{
			"code": "410700",
			"name": "新乡市",
			"level": "city",
			"children": [{
					"code": "410704",
					"name": "凤泉区",
					"level": "district",
					"children": [{
							"code": "410704",
							"name": "潞王坟乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410704",
							"name": "大块镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410704",
							"name": "宝东街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410704",
							"name": "宝西街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410704",
							"name": "耿黄镇",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410783",
					"name": "长垣市",
					"level": "district",
					"children": [{
							"code": "410783",
							"name": "恼里镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410783",
							"name": "南蒲街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410783",
							"name": "佘家镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410783",
							"name": "赵堤镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410783",
							"name": "孟岗镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410783",
							"name": "张三寨镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410783",
							"name": "蒲西街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410783",
							"name": "魏庄街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410783",
							"name": "方里镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410783",
							"name": "常村镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410783",
							"name": "樊相镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410783",
							"name": "蒲北街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410783",
							"name": "满村镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410783",
							"name": "蒲东街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410783",
							"name": "丁栾镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410783",
							"name": "苗寨镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410783",
							"name": "芦岗乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410783",
							"name": "武邱乡",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410711",
					"name": "牧野区",
					"level": "district",
					"children": [{
							"code": "410711",
							"name": "和平路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410711",
							"name": "新乡化学与物理电源产业园区",
							"level": "street",
							"children": []
						},
						{
							"code": "410711",
							"name": "北干道街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410711",
							"name": "东干道街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410711",
							"name": "王村镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410711",
							"name": "新辉路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410711",
							"name": "荣校路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410711",
							"name": "卫北街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410711",
							"name": "牧野乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410711",
							"name": "花园街道",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410703",
					"name": "卫滨区",
					"level": "district",
					"children": [{
							"code": "410703",
							"name": "平原镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410703",
							"name": "健康路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410703",
							"name": "解放路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410703",
							"name": "中同街街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410703",
							"name": "南桥街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410703",
							"name": "铁西街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410703",
							"name": "自由路街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410703",
							"name": "胜利路街道",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410727",
					"name": "封丘县",
					"level": "district",
					"children": [{
							"code": "410727",
							"name": "应举镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410727",
							"name": "城关乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410727",
							"name": "荆宫乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410727",
							"name": "赵岗镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410727",
							"name": "陈桥镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410727",
							"name": "黄德镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410727",
							"name": "李庄镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410727",
							"name": "潘店镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410727",
							"name": "鲁岗镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410727",
							"name": "王村乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410727",
							"name": "陈固镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410727",
							"name": "居厢镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410727",
							"name": "黄陵镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410727",
							"name": "尹岗镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410727",
							"name": "城关镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410727",
							"name": "冯村乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410727",
							"name": "回族乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410727",
							"name": "曹岗乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410727",
							"name": "留光镇",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410724",
					"name": "获嘉县",
					"level": "district",
					"children": [{
							"code": "410724",
							"name": "史庄镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410724",
							"name": "位庄乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410724",
							"name": "照镜镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410724",
							"name": "亢村镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410724",
							"name": "太山乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410724",
							"name": "冯庄镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410724",
							"name": "徐营镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410724",
							"name": "城关镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410724",
							"name": "大新庄乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410724",
							"name": "黄堤镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410724",
							"name": "西工区管理委员会",
							"level": "street",
							"children": []
						},
						{
							"code": "410724",
							"name": "中和镇",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410725",
					"name": "原阳县",
					"level": "district",
					"children": [{
							"code": "410725",
							"name": "福宁集镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410725",
							"name": "韩董庄镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410725",
							"name": "齐街镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410725",
							"name": "龙源街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410725",
							"name": "靳堂乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410725",
							"name": "官厂镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410725",
							"name": "原武镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410725",
							"name": "陡门乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410725",
							"name": "大宾镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410725",
							"name": "蒋庄乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410725",
							"name": "师寨镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410725",
							"name": "阳阿乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410725",
							"name": "桥北乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410725",
							"name": "路寨乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410725",
							"name": "祝楼乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410725",
							"name": "太平镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410725",
							"name": "葛埠口乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410725",
							"name": "阳和街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410725",
							"name": "原兴街道",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410721",
					"name": "新乡县",
					"level": "district",
					"children": [{
							"code": "410721",
							"name": "大召营镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410721",
							"name": "七里营镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410721",
							"name": "小冀镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410721",
							"name": "翟坡镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410721",
							"name": "合河乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410721",
							"name": "古固寨镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410721",
							"name": "新乡经济开发区",
							"level": "street",
							"children": []
						},
						{
							"code": "410721",
							"name": "朗公庙镇",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410702",
					"name": "红旗区",
					"level": "district",
					"children": [{
							"code": "410702",
							"name": "开发区关堤乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410702",
							"name": "洪门镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410702",
							"name": "东街街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410702",
							"name": "开发区街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410702",
							"name": "小店镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410702",
							"name": "向阳小区街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410702",
							"name": "西街街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410702",
							"name": "小店工业园区",
							"level": "street",
							"children": []
						},
						{
							"code": "410702",
							"name": "文化街街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410702",
							"name": "渠东街道",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410781",
					"name": "卫辉市",
					"level": "district",
					"children": [{
							"code": "410781",
							"name": "顿坊店乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410781",
							"name": "孙杏村镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410781",
							"name": "狮豹头乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410781",
							"name": "东风农场",
							"level": "street",
							"children": []
						},
						{
							"code": "410781",
							"name": "安都乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410781",
							"name": "唐庄镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410781",
							"name": "太公镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410781",
							"name": "农科所",
							"level": "street",
							"children": []
						},
						{
							"code": "410781",
							"name": "后河镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410781",
							"name": "柳庄乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410781",
							"name": "苗圃场",
							"level": "street",
							"children": []
						},
						{
							"code": "410781",
							"name": "李源屯镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410781",
							"name": "城郊乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410781",
							"name": "庞寨乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410781",
							"name": "五四农场",
							"level": "street",
							"children": []
						},
						{
							"code": "410781",
							"name": "汲水镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410781",
							"name": "上乐村镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410781",
							"name": "原种场",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410782",
					"name": "辉县市",
					"level": "district",
					"children": [{
							"code": "410782",
							"name": "城关街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410782",
							"name": "峪河镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410782",
							"name": "高庄乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410782",
							"name": "薄壁镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410782",
							"name": "赵固乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410782",
							"name": "百泉镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410782",
							"name": "北云门镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410782",
							"name": "孟庄镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410782",
							"name": "吴村镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410782",
							"name": "占城镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410782",
							"name": "上八里镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410782",
							"name": "常村镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410782",
							"name": "胡桥街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410782",
							"name": "西平罗乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410782",
							"name": "洪洲乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410782",
							"name": "拍石头乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410782",
							"name": "张村乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410782",
							"name": "黄水乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410782",
							"name": "南寨镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410782",
							"name": "南村镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410782",
							"name": "冀屯镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410782",
							"name": "沙窑乡",
							"level": "street",
							"children": []
						}
					]
				},
				{
					"code": "410726",
					"name": "延津县",
					"level": "district",
					"children": [{
							"code": "410726",
							"name": "文岩街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410726",
							"name": "僧固乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410726",
							"name": "位邱乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410726",
							"name": "东屯镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410726",
							"name": "马庄乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410726",
							"name": "司寨乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410726",
							"name": "胙城乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410726",
							"name": "王楼乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410726",
							"name": "潭龙街道",
							"level": "street",
							"children": []
						},
						{
							"code": "410726",
							"name": "丰庄镇",
							"level": "street",
							"children": []
						},
						{
							"code": "410726",
							"name": "榆林乡",
							"level": "street",
							"children": []
						},
						{
							"code": "410726",
							"name": "石婆固乡",
							"level": "street",
							"children": []
						}
					]
				}
			]
		}
	]
}]
export default location
