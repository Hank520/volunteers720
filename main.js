import Vue from 'vue'
import App from './App'
Vue.config.productionTip = false

App.mpType = 'app'

//uView
import uView from "uview-ui";

//自定义头部导航
import navbar from "components/common/head.vue";
Vue.component("navbar", navbar);

//uView
Vue.use(uView);

const app = new Vue({
	...App
})
app.$mount()
