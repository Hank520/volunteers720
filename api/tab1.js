/* tab1主页相关接口 */
import request from '../util/http.js'

/* 通知公告接口 */
export function gongGaoList(params){
	return request.get(
		"/api/rescue/gongGaoList",
		params
	)
}

export function detail(params) {
	return request.get(
		'/api/rescue/notice/queryById',
		params
	)
}