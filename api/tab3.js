/* tab1主页相关接口 */
import request from '../util/http.js'

/* 咨询 */
export function consultList(params){
	return request.get(
		"/api/rescue/consultingList",
		params
	)
}

/* 上传志愿者现金工作个人申请提交 */
export function addAdvanced(params){
	return request.post(
		"/api/rescue/advanced/add",
		params
	)
}

/* 上传志愿者现金工作个人申请修改 */
export function editAdvanced(params){
	return request.post(
		"/api/rescue/advanced/edit",
		params
	)
}

/* 上传志愿者现金工作个人申请获取详情 */
export function queryByOpenIdAdvanced(params){
	return request.get(
		"/api/rescue/advanced/queryByOpenId",
		params
	)
}

// export function consultList(params){
// 	return request.get(
// 		"/api/rescue/consultingList",
// 		params
// 	)
// }