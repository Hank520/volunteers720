/* tab1主页相关接口 */
import request from '../util/http.js'

/* 通知公告接口 */
export function taskList(params){
	return request.get(
		"/rescue/hnjzRescueInformation/list",
		params
	)
}

export function addTask(params){
	return request.post(
		"/rescue/hnjzRescueInformation/add",
		params
	)
}

