const BASE_URL = 'https://chewei.mynatapp.cc'
export const myRequest = (options) => {
	return new Promise((reslove, reject) => {
		uni.request({
			url: BASE_URL + options.url,
			method: options.method || 'GET',
			data: options.data || {},
			success: (res) => {
				if (res.data.status !== 200) {
					return uni.showToast({
						title: "数据获取失败"
					})
				}
				reslove(res)
			},
			fail:(err)=> {
				uni.showToast({
					title: "请求接口失败"
				})
				reject(err)
			}
		})
	})
}