import config from './config.js';

export default {
	// 全局配置
	common: {
		baseUrl: config.baseUrl,
		header: {
			'Content-Type': 'application/json;charset=UTF-8',
		},
		data: {},
		method: 'GET',
		dataType: 'json'
	},
	// 请求 返回promise
	request(options = {}) {
		// 组织参数h5api
		options.url = this.common.baseUrl + options.url;
		options.header = options.header || this.common.header;
		options.data = options.data || this.common.data;
		options.method = options.method || this.common.method;
		//options.dataType = options.dataType || this.common.dataType
		// const Authorization = uni.getStorageSync('Authorization');
		// // // 二次验证 
		// if (Authorization) {
		// 	// 往header头中添加Authorization
		// 	options.header.Authorization = Authorization;
		// }
		
		/* 监听网络状况——得到当前网络 */
		// setTimeout(()=>{
		// 	let thisRoutes = getCurrentPages();
		// 	let netText = "";
		// 	let IsNet = true;
		// 	uni.getNetworkType({
		// 		success: (res) => {
		// 			switch (res.networkType) {
		// 			    case '2g':
		// 					IsNet = false;
		// 			        netText = "您的网速差，请移至网速好的环境";
		// 			        break;
		// 				case '3g':
		// 					IsNet = false;
		// 				    netText = "您的网速差，请移至网速好的环境";
		// 				    break;
		// 			}
		// 			if(!IsNet){
						/* 提示信息-配合uview ui组件使用 */
		// 				thisRoutes[thisRoutes.length - 1].$vm.$refs.uToast.show({
		// 					title: netText,
		// 					type: 'error'
		// 				})
		// 			}
		// 		}
		// 	})
		// },1000)
		
		// 请求
		return new Promise((res, rej) => {
			// 请求中...
			config.env === 'dev' && options;
			uni.request({
				...options,
				success: (result) => {
					/* 接口参数导致响应失败 */
					if (result.statusCode == 500) {
						uni.showToast({
							title: result.data.msg,
							icon: "error"
						})
						rej(result.data);
						return;
					}
					
					config.env === 'dev' && result;
					if (result.data.code === 200) {
						return res(result.data);
					}
					
					switch (result.data.code) {
						case 500:
							//token失效
							uni.showToast({
								title: result.data.msg,
								icon: "error"
							})
							break;
						default:
							uni.showToast({
								title: result.data.msg,
								icon: "error"
							})
							break;
					}
					rej(result.data)
				},
				fail: (error) => {
					let thisRoute = getCurrentPages();
					config.env === 'dev' && error;
					thisRoute[thisRoute.length - 1].$vm.$refs.uToast.show({
						title: "请打开您的移动数据或者WLAN",
						type: 'error'
					})
				}
			});
		})
	},

	// get请求
	get(url, data = {}, options = {}) {
		options.url = url
		options.data = data
		options.method = 'GET'
		return this.request(options)
	},
	//post请求  json对象  添加请求头    application/json
	post(url, data = {}, options = {}) {
		options.url = url
		options.data = data;
		options.header = {
			'Content-Type': 'application/json',
		};
		options.method = 'POST'
		return this.request(options)
	},
	// post请求  json字符串  添加请求头    application/x-www-form-urlencoded
	postJsonStringify(url, data = {}, options = {}) {
		options.url = url;
		options.data = data;
		options.header = {
			'Content-Type': 'application/x-www-form-urlencoded',
		};
		options.method = 'POST'
		return this.request(options)
	},

	//上传文件  图片
	uploadFile(url, data, onProgress = false) {
		return new Promise((result, reject) => {
			//当前路由
			let thisRoute = getCurrentPages();
			const uploadTask = uni.uploadFile({
				...data,
				url: this.common.baseUrl + url,
				success: (res) => {
					let data = JSON.parse(res.data);
					if (res.statusCode !== 200 || data.code !== 200) {
						return thisRoute[thisRoute.length - 1].$vm.$refs.uToast.show({
							title: data.msg || '上传失败',
							type: 'error'
						})
						reject(data)
					} else {
						result(data);
					}
				},
				fail: (err) => {
					reject(err)
				}
			})

			uploadTask.onProgressUpdate((res) => {
				if (typeof onProgress === 'function') {
					onProgress(res.progress)
				}
			});

		})
	},
	
	//上传图片
	async upload(data, onProgress) {
		let uploadResult = null;
		uploadResult = await this.uploadFile('/upload/new/newUploadFile', {
			filePath: data.tempFilePath,
			name: 'file'
		});
	
		return uploadResult;
	}
}
