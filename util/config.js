const env = process.env.NODE_ENV === 'development' ? 'dev' : 'prod'

const config = {
	dev: {//测试
		baseUrl: 'http://weixiaokong.91xiaokong.com/jeecg-boot'
		// baseUrl: 'http://ey6pxx.natappfree.cc/jeecg-boot'
	},
	prod: { // 生产环境 
		baseUrl: 'http://weixiaokong.91xiaokong.com/jeecg-boot'
	}
}

export default {
	...config[env],
	env
}